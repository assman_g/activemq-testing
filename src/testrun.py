import contextlib
import datetime
import json
import os
import signal
import subprocess as sub
import sys
import time
from pathlib import Path
# import crystfelparser
# import h5py
# import numpy as np
# import requests
#from loguru import logger
import receive_msg


# ========== functions ================


def main():
    """
    hello world testing
    :return: nothing
    """
    print("hello world")
    pass


def sigint_handler(signum, frame):
    global TERMINATE_SERVER
    print("CTRL-C caught --- Terminating VDP now")
    TERMINATE_SERVER = True


def to_json(obj):
    """
    makes an object serializable for json
    :param obj: class object
    :return: json serialzable object with indent=4
    """
    return json.dumps(obj, default=lambda obj: obj.__dict__, indent=4)


# --------class with functions-----


class CollectedH5:
    def __init__(self, mess_in):
        # dictionary of the json message
        self.message = mess_in

    def get_message_dict(self):
        """
        returns dictionary of the message (json)
        :return: self.message
        """
        return self.message






if __name__ == "__main__":
    # main()

    vdp_server = "sf-broker-01.psi.ch"
    vdp_port = 61613
    vdp_inqueue = "/queue/test_in"
    # logger.info("In_queue is: {}", vdp_inqueue)
    vdp_outqueue = "I_dont_know"
    vdp_listener = receive_msg.MyListener(vdp_server, vdp_port, vdp_inqueue, vdp_outqueue)
    vdp_listener.connect()
    #logger.info("connected to in_queue")
    print("connected to queue apparently")
    TERMINATE_SERVER = False

    #logger.info("\nWaiting for SIGINT to stop...")
    signal.signal(signal.SIGINT, sigint_handler)

    while not TERMINATE_SERVER:
        if vdp_listener.incoming_messages_queue.empty():
            time.sleep(0.1)
            #print("---")
        else:
            # recieves message from queue. function from python package queue. same as empty.
            #logger.info("received message from in_queue, started processing...")
            message = vdp_listener.incoming_messages_queue.get()
            # Do something with the message
            #logger.info(f"message is: {message}")
            mess_inp = CollectedH5(message)
            mess_inp.get_message_dict()
            
            vdp_listener.acknowledge(message.headers["ack"])


    vdp_listener.disconnect()
