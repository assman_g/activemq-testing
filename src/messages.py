import os
import time
import json
import copy
import stomp
from pprint import pprint
import uuid


class BaseMessage(object):
    """The base message.

    The base message includes the encoding/decoding methods.
    """

    def __init__(self, message=None):
        self.trackingId = None
        if type(message) is str:
            self.decodeJson(message)
        elif type(message) is dict:
            self.__dict__.update(message)
        elif isinstance(message, BaseMessage):
            self.__dict__.update(message.as_dict())

    def encodeJson(self):
        return json.dumps(self, default=lambda self: self.__dict__)

    def decodeJson(self, message):
        self.__dict__.update(json.loads(message))

    def as_dict(self):
        return copy.copy(self.__dict__)

    def _dump(self, as_json=False):
        if as_json:
            pprint(json.dumps(self.__dict__, indent=4, sort_keys=True))
        else:
            pprint(self.__dict__)

    def __getitem__(self, item):
        return self.__dict__[item]

    def __setitem__(self, item, value):
        self.__dict__[item] = value

    def __repr__(self):
        x = type(self)
        mro = str(x.mro())
        s = "<" + mro
        for k, v in list(self.__dict__.items()):
            s = s + ("\n%20s : %s" % (k, str(v)))
        s = s + "\n>\n"
        return s

