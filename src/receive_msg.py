# Adapted from J.W 2023
# class with the functions to connect to the queue and recieve the message

import copy
import queue
import time

import stomp
from loguru import logger

# uncomment at beamline, comment onRA
# TODO check if also available on RA - so no switching required
import messages


class MyListener(stomp.ConnectionListener):
    """Mylistener class"""

    def __init__(self, server, port, inqueue, outqueue):
        self.server = server
        self.port = port
        self.inqueue = inqueue
        self.outqueue = outqueue
        self.incoming_messages_queue = queue.Queue(maxsize=0)

    def connect(self):
        """Connect and subscribe to the inqueue"""

        self.conn = stomp.Connection12([(self.server, self.port)])
        self.conn.set_listener("", self)
        self.conn.connect()
        headers = {"activemq.prefetchSize": 1}
        self.conn.subscribe(destination=self.inqueue, id=1, ack="client", headers=headers)

    def disconnect(self):
        """Close connection"""

        self.conn.disconnect()

    def on_error(self, message):
        pass

    def send(self, outqueue, message):
        """Send message to the outqueue
        Send takes queue, body, content_type, headers and keyword_headers"""

        message = messages.BaseMessage(message)
        self.conn.send(destination=outqueue, body=message.encodeJson())
        time.sleep(1.0)
        # self.conn.unsubscribe(id=1)

    def on_message(self, message):
        """Upon receiving message put it into incoming queue"""

        logger.info("message is (on_message function) {}".format(message))
        try:
            m = messages.BaseMessage(message.body)
        except BaseException as e:
            logger.info("Exception occurred: {}".format(e))
            return
        if hasattr(m, "trackingId"):
            m.headers = copy.copy(message.headers)
            self.incoming_messages_queue.put(m)
            logger.info("Received and processing message {}".format(m.trackingId))

    def acknowledge(self, ack_id):
        """Acknowledge message dequeues it"""

        self.conn.ack(ack_id)
