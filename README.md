# activemq-testing 

Testing messaging  with ActiveMQ broker instance via port forwarding on localhost ( if Message broker is in non-standard PSI network - this example :SwissFEL network)

# Prerequisites:
- Virtual Machine (this example: sf-broker-01.psi.ch) with an running ActiveMQ broker instance (how to setup ActiveMQ on a VM: https://gitlab.psi.ch/assman_g/mx-ansible)
- ssh connection to the VM over wmgt01
- conda env with the following packages: 
    - stomp.py
    - loguru (if logging should be switched on)

# Usage
### Accessing ActiveMQ web interface of the broker instance
- open a terminal and do ```ssh -J wmgt -L 8161:localhost:8161 sf-broker-01``` (port forwarding in order to open the ActiveMQ web interface )
- open localhost:8161 on your local machine in the browser, ActiveMQ interface should open up (Login credentials: admin, artemis)

### Sending and receiving messages 
- open a terminal and do ```ssh -J wmgt -L 61613:localhost:61613 sf-broker-01``` (port forwarding in order to enable messaging via STOPM (port 61613) )
- open another terminal and run ``` python testrun.py ``` . It will 
    - connect to the server, where the broker instance is running
    - connect to port 61613 
    - listen to the queue/topic that is specified ( change accoringly!) as long as a message is received or the connection is ended with CTRL C
- open a terminal and run ``` python send_msg.py ``` to submit a message to the queue. The "listener: terminal will output the received message, if successful. 

# Notes
- make sure all the ssh connections work, before setting up port forwarding etc. 
