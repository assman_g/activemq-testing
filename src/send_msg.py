import copy
import json
import sys
import time
import messages
import stomp

# ------FUNCTIONS -------------


def main():
    """
    hello world testing
    :return: nothing
    """
    print("hello snake ")
    pass


# -------CLASS  -------------------


class MySender:
    """
    Class to connect to queue and send message
    """

    def __init__(self, server, port):
        self.server = server
        self.port = port

    def connect(self):
        """Connect and subscribe to the inqueue"""

        self.conn = stomp.Connection12([(self.server, self.port)])
        self.conn.connect()

    def disconnect(self):
        """Close connection"""

        self.conn.disconnect()

    def on_error(self, headers, message):
        pass

    def send(self, queue, message):
        """Send message to the queue"""

        message = messages.BaseMessage(message)
        # print out message, comment in real scenario:
        # print(message.as_dict())
        # send message, uncomment in real scenario
        self.conn.send(destination=queue, body=message.encodeJson())
        time.sleep(0.2)

    def unsubscribe(self):
        """Unsubscribe from the queue"""

        self.conn.unsubscribe(id=2)


# =============MAIN====================

if __name__ == "__main__":
    main()
    vdp_server = "sf-broker-01.psi.ch"
    vdp_port = 61613
    # queue name ? - beamline specific ?
    vdp_queue = "/queue/test_in"
    #data_number = "%02d" % float(sys.argv[1])
    data_number = "pups"
    # -------------CHOSE Test Message ----------
    # LYSO
    vdp_msg_lyso = {
        "mergeID": "something_mergeID_EP",
        "trackingId": "something_track_EP",
        "eaccount": "e20233",
        "masterFileName": "/20230228/EP2/run000024_EP2_1kHz_980us_without_laser_master.h5",
        "dataFileName": "/20230228/EP2/run000024_EP2_1kHz_980us_without_laser_data_0" + data_number + ".h5",
        "filesystemPath": "/sls/MX/Data10/e20233/",
        "detectorDistance_mm": 150.8,
        "beamCenterX_pxl": 1135.5,
        "beamCenterY_pxl": 1159.1,
        "pixelSize_um": 75,
        "numberOfImages": 6250,
        "imageTime_us": 100,
        "enery_kev": 12.4,
        "detectorWidth_pxl": 2067,
        "detectorHeight_pxl": 2163,
        "underload": -30000,
        "overload": 30000,
        "unitCell": {"a": 45.8, "b": 73.9, "c": 53.5, "alpha": 90.0, "beta": 109.6, "gamma": 90.0},
        "spaceGroupNumber": 4,
        "crystfelTreshold": 7.0,
        "crystfelMinSNR": 3.0,
        "crystfelMinPixCount": 1,
        "crystfelMultiCrystal": False,
    }

    # testing on RA
    # initialze MySender class with server and port and connect:
    print("before connecting")
    vdp_sender = MySender(vdp_server, vdp_port)
    vdp_sender.connect()
    print("after connecting")
    # send message to queue:
    vdp_sender.send(vdp_queue, vdp_msg_lyso)
    print("after sending")
    vdp_sender.disconnect()
